# Gitlab CI

## Définitions
---
    Pipeline > Runners > Stages > Jobs > Scripts

## I. Installation d'un runner dans un container
---

### Documentation officielle

> [Doc gitlab](https://docs.gitlab.com/runner/install/docker.html)

### A. Créer son container d'après l'image gitlab-runner

> [Image gitlab-runner sur docker hub](https://hub.docker.com/r/gitlab/gitlab-runner)

> [Doc Gitlab pour installer son runner](https://docs.gitlab.com/runner/install/docker.html)
```
docker pull gitlab/gitlab-runner
```

```
docker run -d --name gitlab-runner --restart always `
    -v /var/run/docker.sock:/var/run/docker.sock `
    -v gitlab-runner-config:/etc/gitlab-runner `
    gitlab/gitlab-runner:latest
```
### B. Enregistrer son runner

> [Doc Gitlab pour enregistrer son runner](https://docs.gitlab.com/runner/register/index.html#docker)

```
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```

## II. Se connecter à un serveur distant depuis un pipeline GitLab
---

### Introduction
> Il faut donner à Gitlab une clé SSH générée par le serveur distant.
https://docs.gitlab.com/ee/user/ssh.html

### A. Générer une paire de clé SSH depuis le serveur distant
```
ssh-keygen -t rsa -b 4096
```
### B. Ajouter la clé publique à votre compte GitLab
```
cat ~/.ssh/id_rsa.pub
```
>- Copiez la clé publique depuis le fichier *id_rsa.pub*.
>- Enregistez la dans votre compte GitLab en cliquant sur votre photo de profile puis en allant dans "*Settings > SSH Keys*"

### C. Ajouter la clé privée aux variables d'environnement GitLab

>- Dans "*Settings > CI/CD > Variables*", créer une variable *SSH_PRIVATE_KEY*  
>- Mettre la variable en "**protected**".  
>- Selectionner **file**.

### D. Configurer les fichiers clé privée et clé publique sur le serveur distant

```
sudo chmod  600 ~/.ssh/id_rsa
sudo chmod  600 ~/.ssh/id_rsa.pub
```
> En cas de mauvaise configuration on peut obtenir une erreur comme celle-ci: *"Permissions 0666 for '/builds/myrepository/pipeline.tmp/SSH_PRIVATE_KEY' are too open."*.

### E. Enregistrer la clé dans le fichier authorized-keys sur le serveur distant
> Copier la clé publique à la fin du fichier *authorized-keys*.
```
sudo vim ~/.ssh/authorized-keys
```

> En cas de mauvaise configuration on peut obtenir une erreur comme celle-ci: *"user@ip: Permission denied (publickey,password)"*.

### F. Configurer le fichier de la clé privée dans le container
> Dans le fichier *.gitlab-ci.yml*, mettre un *before script* pour restreindre les droits de la clé dans le container.
```
before_script:
    - chmod 600 $SSH_PRIVATE_KEY
```
> En cas de mauvaise configuration on peut obtenir une erreur comme celle-ci: *"Permissions 0666 for '/builds/myrepository/pipeline.tmp/SSH_PRIVATE_KEY' are too open."*.

### G. Se connecter au serveur depuis le pipeline
```
script:
    - ssh -o StrictHostKeyChecking=no -i $SSH_PRIVATE_KEY -p <port> <user>@<ip> "
      echo "Hello-world"
      "
```