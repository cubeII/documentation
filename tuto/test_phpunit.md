# Configurer et utiliser PHPUnit dans un pipeline

## I. Qu'est-ce que PHPUnit

PHPUnit est un framework de tests unitaires open-source pour PHP. Il nous permet de créer des tests automatisés pour vérifier le comportement des fonctions et méthodes PHP, afin de nous assurer qu'elles fonctionnent comme prévu.

Pour plus d'informations, voir [la documentation officielle PHPUnit](https://phpunit.de/documentation.html).

## II. Installer PHPUnit

### A. Vérifier les versions PHP supportées par PHPUnit

<!-- ![Texte alternatif](../img/versions_phpunit.png) -->
<img src="../img/versions_phpunit.png" width="700">

[Source: documentation officielle](https://phpunit.de/supported-versions.html)


Nous utiliserons **PHPUnit 9** pour rester compatible avec l'environnement PHP 7.4 du projet vide-grenier.

### B. Installer PHPUnit

Dans le fichier **composer.json** du projet vide-grenier, on peut voir qu'une version est déjà définie pour PHPUnit. La règle **"^9.6"** signifie que le projet peut utiliser toutes les versions ultérieures tant qu'elles ne dépassent pas la version 9.

```json
{
    "require": {
        "twig/twig": "~3.0",
        "ext-pdo": "*",
        "ext-json": "*",
        "php": "^7.4",
        "phpunit/phpunit": "^9.6"
    },
    "autoload": {
        "psr-4": {
            "Core\\": "Core/",
            "App\\": "App/"
        }
    }
}
```
Mettez à jour les dépendances composer pour installer PHPUnit sur votre poste.

```
composer update
```

Entrez ensuite la commande suivante pour vérifier que la bibliothèque se soit bien installée: 

```
./vendor/bin/phpunit --version
```

<!-- ![Texte alternatif](../img/cli_phpunit.png) -->
<img src="../img/cli_phpunit.png" width="600">

## III. Comment utiliser PHPUnit
### A. Configurer PHPUnit

Le fichier de configuration **phpunit.xml** permet de spécifier divers paramètres et options pour exécuter les tests de manière personnalisée. Vous trouverez ce fichier à la racine du projet.

Vous pouvez notamment ajouter des options de configuration pour définir le déroulement des test, définir la couverture des tests (les dossiers à exclure par exemple) ou encore formater les rapports d'activité.

Pour ajouter de nouvelles options, ajoutez des attributs à la balise xml **phpunit** comme ci-dessous :
```xml
<?xml version="1.0" encoding="UTF-8"?>
<phpunit bootstrap="vendor/autoload.php"
    colors="true"
    stopOnFailure="false"
    //ajouter vos options ici
    //...
    //...
    >

    <testsuites>
        <testsuite name="default">
            <directory>tests</directory>
        </testsuite>
    </testsuites>
    
</phpunit>
```
Quelques exemples d'options:
- **bootstrap** : spécifie le chemin d'accès au fichier d'autoload des dépendances pour le projet.
- **colors** : affiche les résultats des tests en couleurs pour une meilleure lisibilité.
- **stopOnFailure** : arrête l'exécution des tests après la première erreur ou échec rencontré.

Vous retrouverez la liste complète des options sur la page dédiée de la [documentation officielle](https://docs.phpunit.de/en/9.6/configuration.html?highlight=xml).


### B. Implémenter les tests

Les fichiers de tests sont localisés dans le **dossier test** situé à la racine du projet. Chaque fichier est dédié aux tests d'une classe en particulier et doit être nommé d'après elle.
>Pour tester une classe *MaClasse*, créez un nouveau fichier *MaClasseTest.php* et ajoutez le au dossier test.

Dans votre fichier, votre classe doit étendre la classe **TestCase** comme ci-dessous :

```php
<?php

class MaClasseTest extends \PHPUnit\Framework\TestCase {
    
    public function testMaFonction(){
        // ...
    }
}
```

Vous pouvez ensuite ajouter autant de fonctions que vous avez de fonctionnalités à tester. Par soucis de clarté, une fonction doit se limiter au test d'une fonctionnalité en particulier.

> Vos fonctions de test doivent obligatoirement commencer par le **préfixe test**, sinon elles ne seront pas prises en compte lors de l'exécution des tests.

Exemple d'implémentation d'un test simple:

```php
<?php

class MaClasseTest extends \PHPUnit\Framework\TestCase {

    public function testMultiplicationParDeux()
    {
        // on instancie la classe à tester
        $maClasse = new MaClasse();

        // pour une valeur de 5, on attend un résulat de 10
        $input = 5;
        $expectedOutput = 10;

        // on éxecute la fonction à tester et on stocke le résultat
        $output = $maClasse->multiplieParDeux($input);

        // on vérifie que le résultat est conforme aux attentes
        $this->assertEquals($expectedOutput, $output);
    }
}
```
PHPUnit met à disposition de nombreuses fonctions pour réaliser nos tests. Dans cet exemple nous utilisons la méthode **assertEquals** qui permet de comparer deux valeurs et de vérifier si elles sont égales. Si les deux valeurs sont identiques, le test réussit, sinon le test échoue.

 Quelques exemples de méthodes permettant de tester le type d'une variable : 

- **assertIsString** : vérifie si la variable donnée est une chaîne de caractères.

- **assertIsNumeric** : vérifie si la variable donnée est un nombre.

- **assertIsArray** : vérifie si la variable donnée est un tableau.

- **assertIsBool** : vérifie si la variable donnée est un booléen.

- **assertIsObject** : vérifie si la variable données est un objet.

 Quelques exemples de méthodes permettant d'utiliser les opérateurs de comparaison :

- **assertEquals** : Compare la valeur attendue à la valeur donnée.

- **assertTrue** / **assertFalse** : Vérifie que la condition est vraie ou fausse.

- **assertSame** / **assertNotSame** : Compare la valeur attendue à la valeur donnée en utilisant l'opérateur === / !==

- **assertNull** / **assertNotNull**: Vérifie que la valeur donnée est nulle ou non.

- **assertContains** / **assertNotContains**: Vérifie qu'un élément est présent ou non dans un tableau.

Vous pouvez retrouver la liste complète des assertions sur la page dédiée de la [documentation officielle](https://docs.phpunit.de/en/9.6/writing-tests-for-phpunit.html).

### C. Lancer les tests en local

Pour lancer les tests sur votre poste, lancez la commande ci-dessous :
```
./vendor/bin/phpunit
```

<!-- ![Texte alternatif](../img/result_test.png) -->
<img src="../img/result_test.png" width="600">


### D. Formater les rapports

En ligne de commande, vous pouvez ajouter des options supplémentaires lorsque vous lancez vos tests pour formater votre rapport. Quelques exemples d'options:  

- **--testdox** : Affiche les résultats des tests dans un format plus lisible

- **--verbose** : Affiche les résultats des tests avec des informations supplémentaires, telles que le temps d'exécution de chaque test.

- **--coverage-text** : Affiche un rapport de couverture de code dans la console.

- **--log-junit nomDeFichier** : Génère un rapport au format XML.

Par exemple, la commande ci-dessous donnera ce résultat :
```
./vendor/bin/phpunit --testdox --verbose
```
<!-- ![Texte alternatif](../img/result_test_2.png) -->
<img src="../img/result_test_2.png" width="600">

## IV. Lancer les tests automatiquement dans un pipeline Gitlab

### A. Configurer un job test

Le fichier **.gitlab-ci.yml** situé à la racine du projet permet de configurer le pipeline. Vous pouvez y configurer les différents *jobs* (c'est à dire les étapes) qui s'éxecuteront et l'ordre dans lequel les lancer.

Ces différentes étapes sont déclarées au début du fichier :

```yml
stages:
  - build-composer
  - build-sass
  - test
  - build
  - delivery
```

Le job test est très léger puisqu'il éxecute uniquement la commande pour lancer les tests phpunit. Les tests s'éxecutent ensuite dans le runner hebergé sur les serveurs de gitlab.

Ce job exécutera les tests à chaque commits et chaque merge.

```yml
# Lance les tests PHPUnit
test:
  stage: test
  image: composer:1.10.1
  script:
    - vendor/bin/phpunit
  allow_failure: false
```

### B. Suivre le déroulement des tests sur gitlab

Après chaque push ou merge, le pipeline est lancé. Vous pouvez suivre son déroulement dans l'onglet **CI/CD > Pipelines**. 

<img src="../img/gitlab_ci.png" width="200">

Pour chaque commit et chaque merge, le tableau de bord vous donne un résumé du déroulement du pipeline. Chaque bulle correspond à un stage.

<img src="../img/gitlab_ci3.png" width="700">
